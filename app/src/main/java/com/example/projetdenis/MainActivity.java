package com.example.projetdenis;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private boolean partie_en_cour=true;
    private int niveau=1;
    private int salle_a_explorer=16;
    private int salle_explorer;
    private HashMap<Integer, Integer> puissance_du_monstre = new HashMap<>();
    private HashMap<Integer, Integer> image_du_monstre = new HashMap<>();
    private HashMap<Integer, String> nom_du_monstre = new HashMap<>();
    private int potions;
    private int charme;
    private int puissance=100;
    private int point_de_vie=10;
    private int puissance_monstre=150;
    private String nom_joueur="Astérix";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button salle1 = (Button) findViewById(R.id.salle1);
        salle1.setOnClickListener((View.OnClickListener)this);

        Button salle2 = (Button) findViewById(R.id.salle2);
        salle2.setOnClickListener((View.OnClickListener) this);

        Button salle3 = (Button) findViewById(R.id.salle3);
        salle3.setOnClickListener((View.OnClickListener) this);

        Button salle4 = (Button) findViewById(R.id.salle4);
        salle4.setOnClickListener((View.OnClickListener) this);

        Button salle5 = (Button) findViewById(R.id.salle5);
        salle5.setOnClickListener((View.OnClickListener) this);

        Button salle6 = (Button) findViewById(R.id.salle6);
        salle6.setOnClickListener((View.OnClickListener) this);

        Button salle7 = (Button) findViewById(R.id.salle7);
        salle7.setOnClickListener((View.OnClickListener) this);

        Button salle8 = (Button) findViewById(R.id.salle8);
        salle8.setOnClickListener((View.OnClickListener) this);

        Button salle9 = (Button) findViewById(R.id.salle9);
        salle9.setOnClickListener((View.OnClickListener) this);

        Button salle10 = (Button) findViewById(R.id.salle10);
        salle10.setOnClickListener((View.OnClickListener) this);

        Button salle11 = (Button) findViewById(R.id.salle11);
        salle11.setOnClickListener((View.OnClickListener) this);

        Button salle12 = (Button) findViewById(R.id.salle12);
        salle12.setOnClickListener((View.OnClickListener) this);

        Button salle13 = (Button) findViewById(R.id.salle13);
        salle13.setOnClickListener((View.OnClickListener) this);

        Button salle14 = (Button) findViewById(R.id.salle14);
        salle14.setOnClickListener((View.OnClickListener) this);

        Button salle15 = (Button) findViewById(R.id.salle15);
        salle15.setOnClickListener((View.OnClickListener) this);

        Button salle16 = (Button) findViewById(R.id.salle16);
        salle16.setOnClickListener((View.OnClickListener) this);

        reset_salle();
        setPuissanceM();
        salle_image();
        nom_du_monstre();
        potion_et_charme();
        mise_a_jour();
    }

    public void setPuissanceM(){
        Random random = new Random();
        puissance_du_monstre.put(R.id.salle1,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle2,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle3,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle4,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle5,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle6,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle7,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle8,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle9,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle10,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle11,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle12,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle13,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle14,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle15,1+random.nextInt(puissance_monstre)+170*(niveau-1));
        puissance_du_monstre.put(R.id.salle16,1+random.nextInt(puissance_monstre)+170*(niveau-1));
    }

    public void salle_image(){
        image_du_monstre.put(R.id.salle1,R.drawable.salle1);
        image_du_monstre.put(R.id.salle2,R.drawable.salle2);
        image_du_monstre.put(R.id.salle3,R.drawable.salle3);
        image_du_monstre.put(R.id.salle4,R.drawable.salle4);
        image_du_monstre.put(R.id.salle5,R.drawable.salle5);
        image_du_monstre.put(R.id.salle6,R.drawable.salle6);
        image_du_monstre.put(R.id.salle7,R.drawable.salle7);
        image_du_monstre.put(R.id.salle8,R.drawable.salle8);
        image_du_monstre.put(R.id.salle9,R.drawable.salle9);
        image_du_monstre.put(R.id.salle10,R.drawable.salle10);
        image_du_monstre.put(R.id.salle11,R.drawable.salle11);
        image_du_monstre.put(R.id.salle12,R.drawable.salle12);
        image_du_monstre.put(R.id.salle13,R.drawable.salle13);
        image_du_monstre.put(R.id.salle14,R.drawable.salle14);
        image_du_monstre.put(R.id.salle15,R.drawable.salle15);
        image_du_monstre.put(R.id.salle16,R.drawable.salle16);
    }

    public void nom_du_monstre(){
        nom_du_monstre.put(R.id.salle1,"titus longinus lancius");
        nom_du_monstre.put(R.id.salle2,"tiberius bigus baebius");
        nom_du_monstre.put(R.id.salle3,"minus geminus gladius");
        nom_du_monstre.put(R.id.salle4,"bigus rusticus brutus");
        nom_du_monstre.put(R.id.salle5,"julius aetonus romanus");
        nom_du_monstre.put(R.id.salle6,"aplusbegalix");
        nom_du_monstre.put(R.id.salle7,"maximus idius betus ");
        nom_du_monstre.put(R.id.salle8,"julius centurius nenpeuplus");
        nom_du_monstre.put(R.id.salle9,"maximus glorius bellus");
        nom_du_monstre.put(R.id.salle10,"volatus columbus avionus");
        nom_du_monstre.put(R.id.salle11,"julius pompus legionarus");
        nom_du_monstre.put(R.id.salle12,"romanus tortus tankus ");
        nom_du_monstre.put(R.id.salle13,"carotus orelus simplus");
        nom_du_monstre.put(R.id.salle14,"stresus agitatus epilepsus");
        nom_du_monstre.put(R.id.salle15,"richus orus senatorus");
        nom_du_monstre.put(R.id.salle16,"Julius Caius Caesar");
    }

    public void potion_et_charme(){
        Object[] salle = puissance_du_monstre.keySet().toArray();
        potions = (Integer)salle[new Random().nextInt(salle.length)];
        charme = (Integer)salle[new Random().nextInt(salle.length)];
        while(charme == potions)
        {
            charme = (Integer)salle[new Random().nextInt(salle.length)];
        }
    }

    public void reset_puissance_joueur()
    {
        puissance = 100;
    }

    public void reset_pdv(){
        point_de_vie = 10;
    }

    public void reset_message(){
        TextView affichage= (TextView) findViewById(R.id.affichage);
        affichage.setText("");
    }

    public  void reset_niveau(){
        niveau = 1;
    }

    public void reset_salle(){
        salle_a_explorer = 16;
        salle_plein(R.id.salle1);
        salle_plein(R.id.salle2);
        salle_plein(R.id.salle3);
        salle_plein(R.id.salle4);
        salle_plein(R.id.salle5);
        salle_plein(R.id.salle6);
        salle_plein(R.id.salle7);
        salle_plein(R.id.salle8);
        salle_plein(R.id.salle9);
        salle_plein(R.id.salle10);
        salle_plein(R.id.salle11);
        salle_plein(R.id.salle12);
        salle_plein(R.id.salle13);
        salle_plein(R.id.salle14);
        salle_plein(R.id.salle15);
        salle_plein(R.id.salle16);
    }

    public void onClick(View v){

        if(partie_en_cour){

            Button salle = (Button) findViewById(v.getId());
            if(salle.getBackground().getConstantState().equals(getResources().getDrawable(R.drawable.bouton_vide).getConstantState())){
                Toast.makeText(this,"Il n'y a plus de romain ici",Toast.LENGTH_SHORT).show();
            }
            else{
                Intent intent = new Intent(this,SecondActivity.class);
                salle_explorer = v.getId();
                intent.putExtra("point de vie",point_de_vie);
                intent.putExtra("puissance joueur",puissance);
                intent.putExtra("puissance_du_monstre", puissance_du_monstre.get(v.getId()));
                intent.putExtra("image_du_monstre", image_du_monstre.get(v.getId()));
                intent.putExtra("nom_du_monstre", nom_du_monstre.get(v.getId()));
                intent.putExtra("nom_du_joueur",nom_joueur);
                if(salle_explorer == potions)
                {
                    intent.putExtra("bonus",1);
                }
                else if(salle_explorer == charme)
                {
                    intent.putExtra("bonus",2);
                }
                else
                {
                    intent.putExtra("bonus",0);
                }
                startActivityForResult(intent,1);
            }
        }


    }

    private void message(String str)
    {
        TextView message = (TextView) findViewById(R.id.affichage);
        message.setText(str);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(data.getStringExtra("resultat").equals("victoire")){
            puissance = puissance+10;
            salle_a_explorer--;
            if(salle_a_explorer == 0)
            {
                message("Voilà de nouveaux romains !");
                niveau_suivant();
            }
            else {
                message("Ils sont fous ces romains !");
                salle_vide(salle_explorer);
            }
            if(salle_explorer == potions)
            {
                Random random = new Random();
                point_de_vie = point_de_vie + 1 + random.nextInt(3);
            }
            if(salle_explorer == charme)
            {
                Random random = new Random();
                puissance = puissance + 5 + random.nextInt(5);
            }
        }

        if(data.getStringExtra("resultat").equals("defaite")){
            point_de_vie = point_de_vie-3;
            if(point_de_vie <=0){
                message("Vous n'avez pas réussi à protéger le village ... ");
                partie_en_cour = false;
            }
            else{
                TextView message = (TextView) findViewById(R.id.affichage);
                message.setText("Je reprendrai bien un peu de potion magique ...");
            }
            salle_explorer(salle_explorer);
        }

        if(data.getStringExtra("resultat").equals("fuite"))
        {
            point_de_vie = point_de_vie-1;
            if(point_de_vie <=0){
                message("Vous n'avez pas réussi à protéger le village ... ");
                partie_en_cour = false;
            }
            else{
                message("Il ne m'inspire pas confiance celui là ...");
            }
            salle_explorer(salle_explorer);
        }
        mise_a_jour();
    }

    public void boite_de_dialogue(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView nom_text = new TextView(this);
        nom_text.setText("Nom du gaulois :");
        layout.addView(nom_text);

        final EditText nom_field = new EditText(this);
        nom_field.setInputType(1);
        layout.addView(nom_field);

        final TextView point_de_vie_text = new TextView(this);
        point_de_vie_text.setText("Point de vie du gaulois");
        layout.addView(point_de_vie_text);

        final EditText point_de_vie_field = new EditText(this);
        point_de_vie_field.setInputType(2);
        layout.addView(point_de_vie_field);

        final TextView puissance_joueur_text = new TextView(this);
        puissance_joueur_text.setText("Puissance du gaulois");
        layout.addView(puissance_joueur_text);

        final EditText puissance_joueur_field = new EditText(this);
        puissance_joueur_field.setInputType(2);
        layout.addView(puissance_joueur_field);

        final TextView puissance_monstre_text = new TextView(this);
        puissance_monstre_text.setText("Puissance des romains");
        layout.addView(puissance_monstre_text);

        final EditText puissance_monstre_field = new EditText(this);
        puissance_monstre_field.setInputType(2);
        layout.addView(puissance_monstre_field);

        builder
                .setTitle("Paramètres")
                .setView(layout)
                .setIcon(R.drawable.parametre)
                .setPositiveButton("Valider",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
						if(!nom_field.getText().toString().equals("")){
                            nom_joueur = nom_field.getText().toString();
                        }
                        
                        if(!point_de_vie_field.getText().toString().equals("")){
                            if(Integer.parseInt(point_de_vie_field.getText().toString()) > 0)
                                point_de_vie = Integer.parseInt(point_de_vie_field.getText().toString());
                        }

                        if(!puissance_joueur_field.getText().toString().equals(""))
                        {
                            if(Integer.parseInt(puissance_joueur_field.getText().toString()) > 0)
                                puissance = Integer.parseInt(puissance_joueur_field.getText().toString());
                        }

                        if(!puissance_monstre_field.getText().toString().equals("")){
                            if(Integer.parseInt(puissance_joueur_field.getText().toString()) > 0)
                                puissance_monstre = Integer.parseInt(puissance_monstre_field.getText().toString());
                        }

                        setPuissanceM();
                        reset_salle();
                        reset_niveau();
                        partie_en_cour = true;
                        mise_a_jour();
                    }
                })
                .setNegativeButton("Annuler",null)
                .show();
    }

	private void quitter(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder
                .setMessage("Voulez vous quitter ?")
                .setPositiveButton("Oui",new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int which){
						finish();
                    }
                })
                .setNegativeButton("Non",null)
                .show();
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        switch(id){
            case R.id.nouveau:
                reset_jeu();
                return true;
            case R.id.parametres:
                boite_de_dialogue();
                return true;
            case R.id.quitter:
				quitter();
				return true;
            default:
                return false;
        }
    }
    
    private void reset_jeu(){
		reset_salle();
        reset_puissance_joueur();
        reset_pdv();
        reset_message();
        reset_niveau();
        potion_et_charme();
        setPuissanceM();
        partie_en_cour =true;
        mise_a_jour();
	}

    private void salle_explorer(int id)
    {
        Button salle = (Button) findViewById(id);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bouton_explore));
    }

    private void salle_vide(int id)
    {
        Button salle = (Button) findViewById(id);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bouton_vide	));
    }

    private void salle_plein(int id)
    {
        Button salle = (Button) findViewById(id);
        salle.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.bouton_rempli));
    }


    public void niveau_suivant(){
        niveau++;
        reset_salle();
        setPuissanceM();
        potion_et_charme();
        mise_a_jour();
    }


    public void mise_a_jour(){
        TextView puissance_du_joueur = (TextView) findViewById(R.id.puissance);
        puissance_du_joueur.setText(Integer.toString(puissance));

        TextView pdv = (TextView) findViewById(R.id.pointsVie);
        pdv.setText(Integer.toString(point_de_vie));

        TextView niv = (TextView) findViewById(R.id.niveau);
        niv.setText(Integer.toString(niveau));

        TextView salle = (TextView) findViewById(R.id.pièces);
        salle.setText(Integer.toString(salle_a_explorer));
    }



}
