package com.example.projetdenis;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;


public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);

        Button attaque = (Button) findViewById(R.id.attaque);
        attaque.setOnClickListener((View.OnClickListener)this);

        Button fuite = (Button) findViewById(R.id.fuite);
        fuite.setOnClickListener((View.OnClickListener) this);

        TextView vie = (TextView) findViewById(R.id.pointsVieJ);
        String v = Integer.toString(getIntent().getIntExtra("point de vie",0));
        vie.setText(v);

        TextView puissance_J = (TextView) findViewById(R.id.puissanceJ);
        String p = Integer.toString(getIntent().getIntExtra("puissance joueur",0));
        puissance_J.setText(p);

        TextView puissanceAdverssaire = (TextView) findViewById(R.id.puissanceA);
        int puissanceM = getIntent().getIntExtra("puissance_du_monstre",0);
        puissanceAdverssaire.setText(Integer.toString(puissanceM));

        ImageView imageView = (ImageView) findViewById(R.id.image_monstre);
        int image = getIntent().getIntExtra("image_du_monstre",0);
        imageView.setImageResource(image);

        TextView nom_du_monstre = (TextView) findViewById(R.id.nom_du_monstre);
        String nom = getIntent().getStringExtra("nom_du_monstre");
        nom_du_monstre.setText(nom);

        TextView nom_du_joueur = (TextView) findViewById(R.id.nom_du_joueur);
        String nomJoueur = getIntent().getStringExtra("nom_du_joueur");
        nom_du_joueur.setText(nomJoueur);

        int bonus = getIntent().getIntExtra("bonus",0);
        if(bonus == 1)
        {
            ImageView image_potions_ou_charme = (ImageView) findViewById(R.id.image_bonus);
            image_potions_ou_charme.setImageResource(R.drawable.potion);
        }
        if(bonus == 2)
        {
            ImageView image_potions_ou_charme = (ImageView) findViewById(R.id.image_bonus);
            image_potions_ou_charme.setImageResource(R.drawable.charme);
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        Intent intent = new Intent();
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            intent.putExtra("resultat", "fuite");
            setResult(RESULT_OK, intent);
            finish();
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent();

        if (v.getId() == R.id.fuite ) {
            //résultat de la page : fuite puis active le onactivityresult
            intent.putExtra("resultat", "fuite");
            setResult(RESULT_OK, intent);
            finish();
        }

        int puissance_J = getIntent().getIntExtra("puissance joueur",0);
        int puissance_A = getIntent().getIntExtra("puissance_du_monstre",0);

        double calcul = puissance_J * Math.random() - puissance_A * Math.random();

        if (v.getId() == R.id.attaque && calcul >= 0){
            intent.putExtra("resultat", "victoire");
            setResult(RESULT_OK, intent);
            finish();
        }
        if (v.getId() == R.id.attaque && calcul < 0){
            intent.putExtra("resultat","defaite");
            setResult(RESULT_OK,intent);
            finish();
        }

    }


}
